import logoReact from "../Assets/Images/logo.png";

export default function HelloWorld() {
  return (
    <div>
      <p>Learn React</p>
      <img src={logoReact} alt="logo-react" />
    </div>
  );
}
