//CONTOH MENGGUNAKAN PROPS
export default function Cards(props) {
  return (
    <div class="card">
      <p class="title">{props.name}</p>
      <p>{props.job}</p>
    </div>
  );
}
