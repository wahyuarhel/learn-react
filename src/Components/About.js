import { Component } from "react";
// CONTOH MENGGUNAKAN STATE
export default class about extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      count: 0,
    };
  }
  handleChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleButtonClick = () => {
    this.setState({ counst: this.state.count });
  };

  render() {
    return (
      <div>
        <input
          type="text"
          value={this.state.name}
          onChange={this.handleChange}
        />
        <div>{`Hallo, Nama saya adalah ${this.state.name}`}</div>
        <h3>Counter : {this.state.count}</h3>
        <button onClick={this.handleButtonClick}>Klik Ini</button>
      </div>
    );
  }
}
