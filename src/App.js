import React from "react";
import { Fragment } from "react";
import HelloWorld from "./Components/Helloworld";
import About from "./Components/About";
import Cards from "./Components/Cards";
import "./App.css";
import "./Assets/Style/card.css";

const App = () => {
  return (
    <Fragment>
      <main>
        <HelloWorld />
        <About />
        <Cards name="Wahyu Ramadhan" job="FrontEnd Dev" />
        <Cards name="Ariel Noah" job="Singer" />
        <Cards name="Ellon Musk" job="Founder Tesla" />
      </main>
    </Fragment>
  );
};

export default App;
